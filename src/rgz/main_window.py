import re
import sys

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget


class MainWindow(QWidget):
    __REGEX = r"(\s*)([^\s]*)"
    __FIRST = 'first'
    __ANYWHERE = 'anywhere'
    __CHAR_BEFORE = 'Characters before: '
    __CHAR_AFTER = "Characters after: "
    __NUMBER_STR_BEFORE = "Number string before: "
    __NUMBER_STR_AFTER = "Number string after: "

    __UK_TRANSLIT = {
        u'\u0410': 'A', u'\u0430': 'a',
        u'\u0411': 'B', u'\u0431': 'b',
        u'\u0412': 'V', u'\u0432': 'v',
        u'\u0413': 'H', u'\u0433': 'h',
        u'\u0490': 'G', u'\u0491': 'g',
        u'\u0414': 'D', u'\u0434': 'd',
        u'\u0415': 'E', u'\u0435': 'e',
        u'\u0404': {__FIRST: 'Ye', __ANYWHERE: 'Ie'}, u'\u0454': {__FIRST: 'ye', __ANYWHERE: 'ie'},
        u'\u0416': 'Zh', u'\u0436': 'zh',
        u'\u0417': 'Z', u'\u0437': 'z',
        u'\u0418': 'Y', u'\u0438': 'y',
        u'\u0406': 'I', u'\u0456': 'i',
        u'\u0407': {__FIRST: 'Yi', __ANYWHERE: 'I'}, u'\u0457': {__FIRST: 'yi', __ANYWHERE: 'i'},
        u'\u0419': {__FIRST: "Y", __ANYWHERE: 'I'}, u'\u0439': {__FIRST: "y", __ANYWHERE: 'i'},
        u'\u041a': 'K', u'\u043a': 'k',
        u'\u041b': 'L', u'\u043b': 'l',
        u'\u041c': 'M', u'\u043c': 'm',
        u'\u041d': 'N', u'\u043d': 'n',
        u'\u041e': 'O', u'\u043e': 'o',
        u'\u041f': 'P', u'\u043f': 'p',
        u'\u0420': 'R', u'\u0440': 'r',
        u'\u0421': 'S', u'\u0441': 's',
        u'\u0422': 'T', u'\u0442': 't',
        u'\u0423': 'U', u'\u0443': 'u',
        u'\u0424': 'F', u'\u0444': 'f',
        u'\u0425': 'Kh', u'\u0445': 'kh',
        u'\u0426': 'Ts', u'\u0446': 'ts',
        u'\u0427': 'Ch', u'\u0447': 'ch',
        u'\u0428': 'Sh', u'\u0448': 'sh',
        u'\u0429': 'Shch', u'\u0449': 'shch',
        u'\u042e': {__FIRST: 'Yu', __ANYWHERE: 'Iu'}, u'\u044e': {__FIRST: 'yu', __ANYWHERE: 'iu'},
        u'\u042f': {__FIRST: "Ya", __ANYWHERE: 'Ia'}, u'\u044F': {__FIRST: "ya", __ANYWHERE: 'ia'}
    }

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.show()

    def setupUi(self, widget):
        widget.setObjectName("widget")
        widget.resize(680, 500)
        widget.setMinimumSize(QtCore.QSize(680, 500))
        widget.setMaximumSize(QtCore.QSize(680, 500))
        self.splitter_3 = QtWidgets.QSplitter(widget)
        self.splitter_3.setGeometry(QtCore.QRect(20, 20, 641, 451))
        self.splitter_3.setOrientation(QtCore.Qt.Vertical)
        self.splitter_3.setObjectName("splitter_3")
        self.splitter_2 = QtWidgets.QSplitter(self.splitter_3)
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName("splitter_2")
        self.splitter = QtWidgets.QSplitter(self.splitter_2)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.splitter)
        self.plainTextEdit.setMinimumSize(QtCore.QSize(40, 0))
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.textBrowser = QtWidgets.QTextBrowser(self.splitter)
        self.textBrowser.setMinimumSize(QtCore.QSize(40, 0))
        self.textBrowser.setObjectName("textBrowser")
        self.widget = QtWidgets.QWidget(self.splitter_3)
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton = QtWidgets.QPushButton(self.widget)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout.addWidget(self.pushButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setMinimumSize(QtCore.QSize(350, 0))
        self.label.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label.setText("")
        self.label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignTrailing | QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.pushButton.raise_()
        self.plainTextEdit.raise_()
        self.textBrowser.raise_()
        self.label.raise_()
        self.splitter.raise_()
        self.label.raise_()

        self.retranslateUi(widget)
        QtCore.QMetaObject.connectSlotsByName(widget)

    def retranslateUi(self, widget):
        _translate = QtCore.QCoreApplication.translate
        widget.setWindowTitle(_translate("widget", "Transliterator"))
        self.pushButton.setText(_translate("widget", "ОК"))

        self.pushButton.clicked.connect(self.handleButton)

    def handleButton(self):
        str_var = self.plainTextEdit.toPlainText()
        find_res = re.findall(self.__REGEX, str_var)

        res_str = str()
        str_number_before = 0
        str_number_after = 0
        char_len_before = 0
        char_len_after = 0

        i = len(str_var) - 1
        j = 0
        for var in find_res:
            if j == i:
                break
            for k, word in enumerate(var):
                if k == 0:
                    res_str += word
                elif k != 0 and word != '':
                    char_len = len(word)
                    char_len_before += char_len
                    if char_len > 1:
                        str_number_before += 1

                    transliterate_str = self.transliterate(word, self.__UK_TRANSLIT)
                    char_len = len(transliterate_str)
                    char_len_after += char_len
                    if char_len > 1:
                        str_number_after += 1

                    res_str += transliterate_str
            j += 1

        self.label.setText(
            "[{}{}] [{}{}] [{}{} {}{}]".format(self.__CHAR_BEFORE, char_len_before, self.__CHAR_AFTER, char_len_after,
                                               self.__NUMBER_STR_BEFORE, str_number_before, self.__NUMBER_STR_AFTER,
                                               str_number_after))
        self.textBrowser.setText(res_str)

    def transliterate(self, word, translit_table):
        converted_word = str()
        str_ptr = 0
        for char in word:
            transchar = str()
            if char in translit_table:
                translit = translit_table[char]
                if type(translit) is dict:
                    if str_ptr == 0:
                        translit = translit[self.__FIRST]
                    else:
                        translit = translit[self.__ANYWHERE]
                transchar = translit
            else:
                transchar = char
            converted_word += transchar
            str_ptr += 1
        return converted_word


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())

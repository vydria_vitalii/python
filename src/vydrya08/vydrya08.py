#!C:\python\interpreter 3.6.0\python.exe

import cgi

GROU = "group"
FIO = "fio"
FACULTY_NAME = "faculty_name"

FILE1 = 'name.txt'
FILE2 = 'values.txt'
FILE3 = "binary_data.dat"

COUNT_RANGE = 15
OFFSET_BYTE = 13;

OFFSET_BYTE_WHENCE = 6
WHENCE = 0
COUNT_READ_BYTE = 3

form = cgi.FieldStorage()
# extract data
group = form.getfirst(GROU)
fio = form.getfirst(FIO)
faculty_name = form.getfirst(FACULTY_NAME)

print("Content-type: text/html\n")
print("""<!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="windows-1251">
            <title>Lab08</title>
        </head>
        <body>""")

f1 = open(FILE1, "w")
f2 = open(FILE2, "w")
try:
    f1.write("{}\n{}\n{}".format(GROU, FIO, FACULTY_NAME))
    f2.write("{}\n{}\n{}".format(group, fio, faculty_name))
finally:
    f1.close()
    f2.close()

f2 = open(FILE2, "r")
try:
    print("<p>")
    print("{0} read: {1}".format(FILE2, f2.readlines()))
    print("</p>")
finally:
    f2.close()

f3 = open(FILE3, "wb")
try:
    f3.write(bytes(list(range(COUNT_RANGE))))
finally:
    f3.close()

f3 = open(FILE3, "rb")
try:
    f3.seek(OFFSET_BYTE - 1)
    print("<p>{} BYTE: {}</p>".format(OFFSET_BYTE, f3.read(1)))
    f3.seek(OFFSET_BYTE_WHENCE, WHENCE)
    print("<p>read count byte: {}</p>".format(f3.read(COUNT_READ_BYTE)))
finally:
    f3.close()

try:
    assert (isinstance(group, int))
    print("<p>{} value cast to int</p>".format(group))
except AssertionError:
    print("<p>{} value not cast to int</p>".format(group))

print("""</body>
        </html>""")

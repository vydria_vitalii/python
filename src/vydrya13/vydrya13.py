from tkinter import *

class FirstWindow(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.grid()
        self.create_widgets()
        self.buttonClicks = 0
        self.n = 0

    def create_widgets(self):
        self.button = Button(self, text = 'Clicks: 0 Mersenne number: -', command = self.count,
            width = 30, height = 2, bg = '#cccccc', fg = 'green', font = '14')
        self.button.grid()

    def count(self):
        self.buttonClicks += 1
        self.button['text'] = 'Clicks: ' + str(self.buttonClicks) + ' Mersenne number: ' + self.getMersenneNumber()

    def getMersenneNumber(self):
        self.n += 1
        return str(2 ** self.n - 1)

class SecondWindow(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.grid()
        self.create_widgets()
        
    def create_widgets(self):
        Label(self, text = 'Try to unblock the joke', fg = 'green',
              font = 'arial 14').grid(row = 0, column = 0, sticky = W)
        Label(self, text = 'Enter password', fg = 'green',
              font = 'arial 14').grid(row = 1, column = 0, sticky = W)
        self.passwordEntry = Entry(self , bg = '#f0f0ff', font = 'arial 14')
        self.passwordEntry.grid(row = 1, column = 1, sticky = E)
        Label(self, text = 'Start part', fg = 'green',
              font = 'arial 14').grid(row = 2, column = 0, sticky = W)
        self.start = Text(self, width = 20, height = 1, wrap = WORD, bg = '#f0f0ff', font = 'arial 14')
        self.start.grid(row = 2, column = 1)
        self.start.insert(0.0, 'My name is')
        Label(self, text = 'End part', fg = 'green', font = 'arial 14').grid(row = 3, column = 0, sticky = W)
        self.end = Text(self, width = 20, height = 1, wrap = WORD, bg = '#f0f0ff', font = 'arial 14')
        self.end.grid(row = 3, column = 1)
        Button(self, text = 'Get end of the joke', fg = 'green', bd = '3',
               font = 'arial 14', command = self.reveal).grid(row = 4, column = 0, sticky = W) 

    def reveal(self):
        contents = self.passwordEntry.get()
        if contents == 'password':
            message= 'Viktor Vyshkovskyi'
        else:
            message = 'Wrong password!'
        self.end.delete(0.0, END)
        self.end.insert(0.0, message)

class ThirdWindow(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.grid()
        self.create_widgets() 

    def create_widgets (self):
        Label(self, text = 'Choose letter to put them together', font = 'arial 14',
            fg = 'green').grid(row = 0, column = 0, sticky = W)
        self.p = BooleanVar()
        Checkbutton(self, text = 'г', variable = self.p,
            command = self.update_text).grid(row = 1, column = 0, sticky = W)
        self.i = BooleanVar()
        Checkbutton(self, text = 'о', variable = self.i,
            command = self.update_text).grid(row = 2, column = 0, sticky = W)
        self.t = BooleanVar()
        Checkbutton(self, text = 'р', variable = self.t,
            command = self.update_text).grid(row = 3, column = 0, sticky = W)
        self.o = BooleanVar()
        Checkbutton(self, text = 'о', variable = self.o,
            command = self.update_text).grid(row = 4, column = 0, sticky = W)
        self.n = BooleanVar()
        Checkbutton(self, text = 'д', variable = self.n,
            command = self.update_text).grid(row = 5, column = 0, sticky = W)
        self.word = Text(self, fg = 'green', font = 'arial 11', width = 35, height = 1, wrap = WORD)
        self.word.grid(row = 6, column = 0)

    def update_text (self):
        word = ''
        if self.p.get():
            word += 'г'
        if self.i.get():
            word += 'о'
        if self.t.get():
            word += 'р'
        if self.o.get():
            word += 'о'
        if self.n.get():
            word += 'д'
        self.word.delete(0.0, END)
        self.word.insert(0.0, word)

class FourthWindow(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.grid()
        self.create_widgets() 

    def create_widgets(self):
        Label(self, text = 'Choose type to discover literal',
            fg = 'green', font = 'arial 14').grid(row = 0, column = 0, sticky = W)
        self.favorite = StringVar()
        self.favorite.set(None)
        Radiobutton(self, text = 'list', variable = self.favorite, value = '[0, 1, 2]',
            command = self.update_text).grid(row = 1, column = 0, sticky = W)
        Radiobutton(self, text = 'tuple', variable = self.favorite, value = '("a", "b", "c")',
            command = self.update_text).grid(row = 5, column = 0,sticky = W)
        Radiobutton(self, text = 'range', variable = self.favorite, value = '[0, 1, 2, 3, 4]',
            command = self.update_text).grid(row = 3, column = 0,sticky = W)
        Radiobutton(self, text = 'str', variable = self.favorite, value = '"string"',
            command = self.update_text).grid(row = 6, column = 0,sticky = W)
        Radiobutton(self, text = 'array', variable = self.favorite, value = 'array("i", [1, -2, 15, 128])',
            command = self.update_text).grid(row = 7, column = 0,sticky = W)
        Radiobutton(self, text = 'dict', variable = self.favorite, value = 'dict(one=1, two=2, three=3)',
            command = self.update_text).grid(row = 7, column = 0,sticky = W)
        self.result = Text(self, fg = 'green', font = 'arial 10',
            width = 35, height = 1, wrap = WORD)
        self.result.grid(row = 8, column = 0)

    def update_text(self):
        message = 'Literal: '
        message += self.favorite.get()
        self.result.delete(0.0, END)
        self.result.insert(0.0, message)

class Main:
    def __init__(self, master):
        self.master = master
        self.frame = Frame(self.master)
        self.button1 = Button(self.frame, text = 'Click to open all windows!', width = 30,
            height = 2, fg = 'green', bd = '3', font = 'arial 14', command = self.newWindows)
        self.button1.pack()
        self.frame.pack()

    def newWindows(self):
        self.firstWindow = Toplevel(self.master)
        self.firstWindow.title('Working with button')
        self.firstWindow.geometry('330x55+100+50')
        self.firstApp = FirstWindow(self.firstWindow)
        self.secondWindow = Toplevel(self.master)
        self.secondWindow.title('Working with text')
        self.secondWindow.geometry('430x150+100+550')
        self.secondApp = SecondWindow(self.secondWindow)
        self.thirdWindow = Toplevel(self.master)
        self.thirdWindow.title('Working with checkbuttons')
        self.thirdWindow.geometry('300x150+1000+50')
        self.thirdApp = ThirdWindow(self.thirdWindow)
        self.fourthWindow = Toplevel(self.master)
        self.fourthWindow.title('Working with radiobuttons')
        self.fourthWindow.geometry('280x190+1010+500')
        self.fourthApp = FourthWindow(self.fourthWindow)

def main(): 
    root = Tk()
    root.title('Main window')
    root.geometry('300x60+550+300')
    app = Main(root)
    root.mainloop()

main()

﻿#!E:/Program Files/Python36/python.exe
# coding: utf-8
print ( "Content-Type: text/html; charset=utf-8" )
print ()
import pickle
TESTS=[ 'Тест №1. К операциям над множествами НЕ относяться:' \
'<br>1. clear<br> 2. intersection <br>3. union;1' ,
'Тест №2. Множество это НЕ:' \
'<br>1. tuple <br>2. set <br>3. frozenset;1' ,
'Тест №3. Функция НЕ является операцией объединения:' \
'<br>1. set.intersection <br>2. set.update <br>3. set.union;1' ,
'Тест №4. К операциям pickle НЕ относят:' \
'<br>1. rebuild <br>2. load <br>3. dump;1' ,
'Тест №5. Для сериализации НЕ используют' \
'<br>1. XML <br>2. pickle <br>3. textfiles;3' ]

f= open ( 'tests.dat' , 'wb')
pickle.dump(TESTS, f);

f.close()
print ( "It/s done" )

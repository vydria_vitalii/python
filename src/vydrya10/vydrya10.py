﻿#!E:/Program Files/Python36/python.exe
print ( "Access-Control-Allow-Origin: *" )
print ( "Content-Type: text/html; charset=utf-8" )
print ()
import cgi, cgitb, random, pickle
cgitb.enable()

import sys
import codecs
sys.stdout = codecs.getwriter('utf-8')(sys.stdout.detach())
sys.stdin = codecs.getreader('utf-8')(sys.stdin.detach())

H=  "<p style='text-align:center;color:#0000b0; font-size:7mm; font-weight:bold'>"
H2=  "<p style='text-align:center;color:#00b0b0; font-size:7.5mm; font-style:italic; font-weight:bold'>"
D=  "<div style='margin-left:1.5cm;color:#0000b0; font-size:5.7mm'>"

def get_test (file_name): # Функция считывает из файла file_name тесты 
	f= open (file_name, 'rb' ) # и возвращает один из них
	test_list=pickle.load(f)
	f.close()
	test=random.choice(test_list)
	test_list.remove(test)
	f= open ( 'cur_tests.dat' , 'wb' )
	pickle.dump(test_list,f)
	f.close()
	return test

def write_results (user_dict, out_points=0, out_tests=0, out_answers=0): 
	print (D, '<i><b>Результаты тестирования:</b></i>' ,
	'<br>Имя &ndash; ' , user_dict[ 'name' ]) 
	if out_points: 
		print ( '<br>Получено баллов &ndash; ' , 
		user_dict[ 'points' ], ' из ' , user_dict[ 'number_test' ]) 
	if out_tests: 
		print ( '<br>Тесты &ndash; ' )
		for el in user_dict[ 'tests' ]: print (el.split( '.' )[0])
	if out_answers: 
		print ( '<br>Ответы &ndash; ' )
		for el in user_dict[ 'answers' ]: print (el)
	print ( '<br>Оценка &ndash; ' )
	m=user_dict[ 'points' ]
	if m==0: mark= "Неудовлетворительно"
	elif m==1: mark= "Удовлетворительно"
	elif m==2: mark= "Хорошо"
	else : mark= "Отлично"
	print ( '"' , mark, '"' )

max_test=3
data=cgi.parse()
if 'name' in data:
	# От клиента получено имя тестируемого 
	test=get_test( 'tests.dat' )
	print (D, test.split( ';' )[0]) 
	user_dict={ # Словарь данных тестируемого содержит:
				'name' :data[ 'name' ][0], # имя
				'number_test' :1, # порядковй номер очередного теста
				'tests' :[test], # список переданных тестов
				'answers' :[], # список ответов пользователя
				'points' :0 } # число набранных баллов
	f= open ( 'user.dat' , 'wb' )
	pickle.dump(user_dict,f)
	f.close()
if 'answer' in data:
	# От клиента получен ответ тестируемого 
	f= open ( 'user.dat' , 'rb' )
	user_dict=pickle.load(f)
	user_dict[ 'answers' ]+=[data[ 'answer' ][0]]
	tests=user_dict[ 'tests' ]
	etalon=tests[ len (tests)-1:][0].split( ';' )[1]
	if data[ 'answer' ][0]==etalon: 
		user_dict[ 'points' ]+=1
	if user_dict[ 'number_test' ]<5: 
		test=get_test( 'cur_tests.dat' )
		print (D, test.split( ';' )[0]) 
		user_dict[ 'number_test'] +=1
		user_dict[ 'tests'] +=[test]
		f= open ( 'user.dat' , 'wb' )
		pickle.dump(user_dict,f)
		f.close() 
	else : 
		write_results(user_dict,out_points=1,out_tests=1,out_answers=1) 

#Скрипт формирователя-файла с тестами
#!E:/Program Files/Python36/python.exe
# coding: utf-8
print ( "Content-Type: text/html; charset=utf-8" )
print ()
import pickle
TESTS=[ 'Тест №1. К операциям над множествами НЕ относяться:' \
'<br>1. clear<br> 2. intersection <br>3. union;1' ,
'Тест №2. Множество это НЕ:' \
'<br>1. tuple <br>2. set <br>3. frozenset;1' ,
'Тест №3. Функция НЕ является операцией объединения:' \
'<br>1. set.intersection <br>2. set.update <br>3. set.union;1' ,
'Тест №4. К операциям pickle НЕ относят:' \
'<br>1. rebuild <br>2. load <br>3. dump;1' ,
'Тест №5. Для сериализации НЕ используют' \
'фукция func() будет непосредсвтвенно доступна программе?' \
'<br>1. XML <br>2. pickle <br>3. textfiles;3' ]

f= open ( 'tests.dat' , 'wb')
pickle.dump(TESTS, f);

f.close()
print ( "It/s done" )
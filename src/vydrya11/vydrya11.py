from Computer import Computer

def main():
    obj1 = Computer()
    obj1.name_processor = "i7"
    obj1.size_ozu = 16
    obj1.motherboard = "Asus Prime B250-Plus"
    obj1.video_card = "nvidia gtx 1080"

    print(obj1)
    print("static method ", obj1.static())


if __name__ == '__main__':
    main()

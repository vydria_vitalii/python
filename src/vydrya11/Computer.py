from builtins import property


class Computer(object):
    def __init__(self):
        self.__clear_data()

    @property
    def name_processor(self) -> str:
        return self.name_processor

    @name_processor.setter
    def name_processor(self, value: str):
        self.__name_processor = value

    @property
    def size_ozu(self) -> int:
        return self.size_ozu

    @size_ozu.setter
    def size_ozu(self, value: int):
        self.__size_ozu = value

    @property
    def video_card(self) -> str:
        return self.__video_card

    @video_card.setter
    def video_card(self, value: str):
        self.__video_card = value

    @property
    def motherboard(self) -> str:
        return self.__motherboard

    @motherboard.setter
    def motherboard(self, value: str):
        self.__motherboard = value

    def __del__(self):
        self.__clear_data()

    def __clear_data(self):
        self.__name_processor: str = None
        self.__size_ozu: int = None
        self.__video_card: str = None
        self.__motherboard: str = None

    def __str__(self) -> str:
        return "Computer (name_processor  = {0}, size_ozu = {1}, video_card = {2}, motherboard = {3})".format(
            self.__name_processor, self.__size_ozu, self.__video_card, self.__motherboard)

    @staticmethod
    def static() -> str:
        return "class Computer"

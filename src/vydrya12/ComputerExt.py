from builtins import property
from Computer import Computer


class ComputerExt(Computer):
    def __init__(self, value: int):
        super().__init__()
        self.__size_disk = value

    @property
    def size_disk(self) -> int:
        return self.__size_disk

    def __str__(self) -> str:
        return str(super().__str__(), "ComputerExt(size_disk = {0})".format(self.__size_disk))

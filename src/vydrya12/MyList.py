class MyList(object):
    def __init__(self):
        self.__list = list()

    def __getitem__(self, item):
        return self.__list.__getitem__(item)

    def __setitem__(self, key, value):
        self.__list.__setitem__(key, value)

    def last(self):
        return self.__list.__getitem__(self.__list.__len__() - 1)

    def push(self, value):
        self.__list.append(value)

    def __str__(self):
        return self.__list.__str__()
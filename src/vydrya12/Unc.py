class Unc(float):
    __UNC = 35.27396194958

    def __new__(cls, arg):
        arg = arg * Unc.__UNC
        return float.__new__(cls, arg)

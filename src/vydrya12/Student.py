from builtins import property
class Student(object):
    def __init__(self):
        self.__name: str = None
        self.__lastname: str = None
        self.__group: str = None

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, value: str):
        self.__name = value

    @property
    def lastname(self) -> str:
        return self.__lastname

    @lastname.setter
    def lastname(self, value: str):
        self.__lastname = value

    @property
    def group(self) -> str:
        return self.__group

    @group.setter
    def group(self, value: str):
        self.__group = value

    def __str__(self) -> str:
        return "Student(name = {}, lastname = {}, group = {})".format(self.__name, self.__lastname, self.__group)

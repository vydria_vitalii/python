from MyList import MyList
from TestClass import TestClass
from Unc import Unc

FLOAT_UNC = 5.7


def main():
    test = TestClass()
    test.name = "Vitaliy"
    test.size_ozu = 16
    print(test.name)
    print(test.size_ozu)


    print("float: ", FLOAT_UNC, " unc: ", Unc(FLOAT_UNC))

    # list
    my_list = MyList()
    my_list.push(2)
    my_list.push(3)
    print("push ", my_list)
    my_list.__setitem__(0, 1)
    print("__setitem__ ", my_list)
    print("__getitem__(0)", my_list.__getitem__(0))


if __name__ == '__main__':
    main()

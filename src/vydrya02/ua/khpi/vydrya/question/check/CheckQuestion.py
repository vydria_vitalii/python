from abc import ABCMeta, abstractmethod

from ua.khpi.vydrya.question import Question


class CheckQuestion(metaclass=ABCMeta):
    @abstractmethod
    def check(self, question: Question, answer: str) -> bool:
        pass

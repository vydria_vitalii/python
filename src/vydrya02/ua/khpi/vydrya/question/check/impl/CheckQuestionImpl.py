from ua.khpi.vydrya.question import Question
from ua.khpi.vydrya.question.check.CheckQuestion import CheckQuestion


class CheckQuestionImpl(CheckQuestion):

    def check(self, question: Question, answer: str) -> bool:
        for var in question.get_answers():
            if var.lower() == answer.strip().lower():
                return True
        return False

from typing import List


class Question:
    def __init__(self):
        self.__subject: str = None
        self.__answers: List[str] = list()

    @property
    def subject(self) -> str:
        return self.__subject

    @subject.setter
    def subject(self, val: str):
        self.__subject = val

    def add_answer(self, val: str):
        self.__answers.append(val)

    def get_answer(self, index: int) -> str:
        return self.__answers[index]

    def get_answers(self) -> List[str]:
        return self.__answers

    def __hash__(self):
        prime = 31
        result = 1
        result = prime * result + (self.__subject is None) if 0 else hash(self.__subject)
        result = prime * result + (self.__answers is None) if 0 else hash(self.__answers)
        return result

    def __eq__(self, other):
        if other is not None and isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __repr__(self) -> str:
        return "Question: subject: {0}, Answer: {1}".format(self.__subject, self.__answers)

from typing import List

from ua.khpi.vydrya.command.Command import Command
from ua.khpi.vydrya.question import Question
from ua.khpi.vydrya.question.check import CheckQuestion


class ConsoleQuestionCommand(Command):
    def __init__(self, data, check_question: CheckQuestion):
        self.__data: List[Question] = data
        self.__check_question: CheckQuestion = check_question

    def execute(self):
        result: List[bool] = list()
        i = 0
        while i != len(self.__data):
            tmp_question = self.__data[i]
            print(tmp_question.subject + " ")
            result.append(self.__check_question.check(tmp_question, input()))
            i += 1

        print("Результат: {0}".format(result.count(True)))

from typing import List

from ua.khpi.vydrya.command.Command import Command
from ua.khpi.vydrya.command.console.ConsoleQuestionCommand import ConsoleQuestionCommand
from ua.khpi.vydrya.main import FIO, start_splice, print_search, search_char
from ua.khpi.vydrya.question.Question import Question
from ua.khpi.vydrya.question.check.CheckQuestion import CheckQuestion
from ua.khpi.vydrya.question.check.impl.CheckQuestionImpl import CheckQuestionImpl


def main():
    print(FIO)
    print(FIO[start_splice:])
    print(print_search.format(search_char, FIO, FIO.count(search_char)))

    checkQuestion: CheckQuestion = CheckQuestionImpl()
    questions: List[Questionnn] = list()

    question1: Question = Question()
    question1.subject = "Какой класс для создания строк ?"
    question1.add_answer("str")

    question2: Question = Question()
    question2.subject = "Базовый класс для коллекций ?"
    question2.add_answer("Container")

    question3: Question = Question()
    question3.subject = "Класс для представления целых чисел ?"
    question3.add_answer("int")

    question4: Question = Question()
    question4.subject = "Метод разбиения строки по разделителю ?"
    question4.add_answer("split")

    question5: Question = Question()
    question5.subject = "При проверке истенности чему равно значение None ?"
    question5.add_answer("False")

    questions.append(question1)
    questions.append(question2)
    questions.append(question3)
    questions.append(question4)
    questions.append(question5)

    command: Command = ConsoleQuestionCommand(questions, checkQuestion)
    command.execute()


if __name__ == "__main__":
    main()

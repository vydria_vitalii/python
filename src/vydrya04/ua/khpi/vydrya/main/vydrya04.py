from collections import Iterator, Container, Iterable, MutableSequence
from ua.khpi.vydrya.main.any import *
INDEX = 1

def main():
    a_tuple = (3.3, True, [6, 6], (9, 9))
    print(str.__subclasses__())

    print("Iterator a_tuple: ", isinstance(a_tuple, Iterator))
    print("Container a_tuple: ", isinstance(a_tuple, Container))
    print("MutableSequence a_tuple: ", isinstance(a_tuple, MutableSequence))
    print("Iterable a_tuple: ", isinstance(a_tuple, Iterable))
    print("Sequence a_tuple: ", isinstance(a_tuple, Iterable))

    tmp_list = list(a_tuple)
    tmp_list[INDEX] = tmp_list.__class__.__name__
    print("tmp_list ", tmp_list)
    a_tuple = tuple(tmp_list)
    print("a_tuple ", a_tuple)

    a_range = range(20,0,-2)
    for i in a_range:
        print(i, end=" ")



if __name__ == "__main__":
    main()
    game()

from random import choice, randrange
import re

WORDS = ('компьютер', 'программа', 'протокол', 'спецификация')
LEVEL_REGEX = r'[1-3]'

def game():
    level = 1
    word = choice(WORDS)
    correct = word
    ana = __word_anagram(word,level)
    print('''
       Игра "АНАГРАММА"
       Для выхода - нажмите клавишу Enter''')
    print('Вот анаграмма', ana.upper())
    ans = input('Попробуй отгадать слово  ')

    inp_level: bool = False
    while ans.lower() != correct.lower() and ans != '':
        if inp_level:
            valid_input: bool = False
            while not valid_input:
                level = input("\nВведите сложность: 1 - сложно; 2 - средне; 3 - легко: ")
                valid_input = (re.match(LEVEL_REGEX, level) is not None)
            level = int(level)
            ana = __word_anagram(correct, level)
            print('Вот анаграмма', ana.upper())
            ans = input('Попробуй отгадать слово  ')
        else:
            print('Ответ неверный')
            inp_level = True
    if ans.lower() == correct.lower():
        print('Молодец!')
    print('Спасибо за игру')
    input()

def __word_anagram(word: str, level: int) -> str:
    tmp = ''
    i = len(word) / (level + 1)
    while i >= 0:
        pos = randrange(0,len(word))
        i -= 1
        tmp += word[pos]
        word = word[:pos] + word[pos + 1:]
    tmp += word
    return tmp

#!C:\python\interpreter 3.6.0\python.exe

import cgi
import re

DATA1 = "data1"
DATA2 = 'data2'
DATA3 = "data3"

PATTERN_DATA1 = r"^гр.: [1-9]\d* с-т [А-Я]$"
PATTERN_DATA2 = r'^[А-ЯA-Z][a-zа-я]+_[А-ЯA-Z][a-zа-я]+$'
PATTERN_DATA3 = r'^[\d]{3}-[\d]{2}-[\d]{2}$'

form = cgi.FieldStorage()
data1 = form.getfirst(DATA1)
data2 = form.getfirst(DATA2)
data3 = form.getfirst(DATA3)

print("Content-type: text/html\n")
print("""<!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="windows-1251">
            <title>Lab09</title>
        </head>
        <body>""")

print("<p>findall number:", re.findall(r'\d', data1), "</p>")
print("<p>findall list str:", re.findall(r'\S+ \S+ \S+', data2), "</p>")

print(data1.rstrip())
print("<p>data1:", re.match(PATTERN_DATA1, data1) is not None, "</p>")
print("<p>data2:", re.match(PATTERN_DATA2, data2) is not None, "</p>")
print("<p>data3:", re.match(PATTERN_DATA3, data3) is not None, "</p>")

print("""</body>
        </html>""")

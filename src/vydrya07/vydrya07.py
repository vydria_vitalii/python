#!C:\python\interpreter 3.6.0\python.exe

import cgi
import cgitb
import random

NAME = "name"
GROUP1 = "group1"
GROUP2 = "group2"
GROUP3 = "group3"

START_RAND = -12
END_RAND = 31
COUNT_RAND = 40


def rand_array(count, start_rand, end_rand):
    i = 0
    res = []
    while (i < count):
        res.append(random.randrange(start_rand, end_rand));
        i += 1
    return res


def func1(array):
    res_list = list()
    for var in array:
        if var % 2 == 0:
            res_list.append(var)
    return res_list


def func2(array):
    temp = None
    for i in array:
        if i > 0:
            temp = i
    for var in array:
        if var >= 0 and temp > var:
            temp = var
    return temp


def comparator(x, y):
    return cmp(abs(x), abs(y))


def func3(array):
    array.sort(cmp=comparator)
    return a


form = cgi.FieldStorage()
name = form.getfirst(NAME)
group1 = form.getfirst(GROUP1)
group2 = form.getfirst(GROUP2)
group3 = form.getfirst(GROUP3)

rand_data = rand_array(COUNT_RAND, START_RAND, END_RAND)

print("Content-type: text/html\n")
print("""<!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="windows-1251">
            <title>Обработка данных форм</title>
        </head>
        <body>""")

print("<h1>{}!</h1>".format(name))
print("rand ", rand_data)

if group1:
    print("<p>Group1: {}</p>".format(func1(rand_data)))

if group2:
    print("<p>Group2: {}</p>".format(func2(rand_data)))
if group3:
    print("<p>Group3: {}</p>".format(func3(rand_data)))

print("""</body>
        </html>""")

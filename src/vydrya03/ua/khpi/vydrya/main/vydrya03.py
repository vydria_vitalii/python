JOIN = "-"

def main():
    a_list = ["hello", "dog", "cat", 3, 4.5, False, [5, "str"]]
    for val in a_list:
        print(type(val), " value ", val)
    a_list.append("pig")
    print("a_list {0}".format(a_list))
    a_list.reverse()
    print(a_list)
    a_list.extend(["new str1", "new str2"])
    print("a_list extend ", a_list)

    s = "hello"
    print("s ", s)

    print(JOIN.join(s))

    b_list = list(s)
    print("b_list ", b_list)
    print("len ", len(b_list))
    print("min ", (b_list))


if __name__ == "__main__":
    main()

import collections

def func1(list_param):
    square = 0
    cube = 0
    for var in list_param:
        cube += pow(3, var)
        square += pow(2, var)
    print("cube({0}) - square({1}) = {2}".format(cube, square, cube - square))


def func2(dict_param, name_value):
    list_res = list()
    for key, value in dict_param.items():
        if value is name_value:
            list_res.append(key)
    return list_res


def func3(list_param):
    list_res = list()
    for var in list_param:
        if isinstance(var, collections.Iterable):
            for item in var:
                list_res.append(hash(item))
        else :
            list_res.append(hash(var))
    return list_res


def main():
    my_list = [5, 34, 63, 34, 6, 35, 7]
    func1(my_list)
    a_dict = {"pow_func": "pow", "isinstance_func": "isinstance", "abs_func": "abs", "pow_func1": "pow",
              "isinstance_func1": "isinstance", "hash_func": "hash", "len_func": "len"}
    print("func2 result: ", func2(a_dict, "pow"))

    a_list = list([1, 45, 5, ["stt1,array, map"], 5, 3, 33, ["str2", "list", "tuple"]])
    print("a_list", a_list)

    a_list = func3(a_list)
    print("func3 result", a_list)
    a_list.sort()
    a_list.reverse()
    print("sort a_list", a_list)

    str_code = '''i = 66 + 11\nif i > 0:\n\tprint('true')\n\relse:\n\tprint('false')'''
    my_code = compile(str_code, '<string>', 'exec')
    print(type(my_code))
    exec(my_code)

if __name__ == '__main__':
    main()

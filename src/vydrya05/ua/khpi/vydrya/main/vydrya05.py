from collections import Hashable

COUNT = 3
def main():
    a_set = {1, 4.5, "str", '5', 5.6, 1, "str2"}
    print("a_set =", a_set)
    a_set.add((4, 6))
    a_set.discard(1)
    print(isinstance(a_set, Hashable))
    it_ob = list()
    i = 0
    for var in a_set:
        if i == COUNT:
            break
        if isinstance(var, Hashable):
            it_ob.append(var)
        else:
            it_ob.append(tuple(var))
        i += 1
    print("it_ob", it_ob)

    b_set = set(it_ob)
    print("b_set = ", b_set)
    print("intersection", a_set.intersection(b_set))

    a_dict = dict({'gamma': 3, 'alpha': 1, 'beta': 2})
    print("a_dict", a_dict)
    print("setdefault", a_dict.setdefault("gamma1", 4))
    print("a_dict", a_dict)
    print("values", a_dict.values())
    print("pop", a_dict.pop("gamma"))


if __name__ == '__main__':
    main()

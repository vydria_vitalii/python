class BiteData:
    def __init__(self):
        self.__a: int = None
        self.__b: int = None
        self.__c: int = None

    @property
    def c(self) -> int:
        return self.__c

    @c.setter
    def c(self, value):
        self.__c = value

    @property
    def a(self) -> int:
        return self.__a

    @a.setter
    def a(self, value: int):
        self.__a = value

    @property
    def b(self) -> int:
        return self.__b

    @b.setter
    def b(self, value: int):
        self.__b = value

    def __repr__(self):
        return "BitData(a = {0}, b = {1}, c = {2})".format(self.__a, self.__b, self.__c)

    def __eq__(self, other) -> bool:
        if other is not None and isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __hash__(self) -> int:
        prime = 31
        result = 1
        result = prime * result + (self.__a is None) if 0 else hash(self.__a)
        result = prime * result + (self.__b is None) if 0 else hash(self.__b)
        result = prime * result + (self.__c is None) if 0 else hash(self.__c)
        return result

from ua.khpi.vydrya.main.BiteData import BiteData
from ua.khpi.vydrya.main.Data import Data
from ua.khpi.vydrya.main.command import input_command, show_pow, pow_command, cast_to_int_command, \
    show_hex_console_command, input_bite_command, show_bite_data_command, and_bite_operations_command
from ua.khpi.vydrya.main.command.console.ConsoleCommand import ConsoleCommand
from ua.khpi.vydrya.main.command.console.ConsoleInputCommand import ConsoleInputCommand
from ua.khpi.vydrya.main.command.console.ConsoleInputShiftCommand import ConsoleInputShiftCommand
from ua.khpi.vydrya.main.command.console.ShowBiteDataConsoleCommand import ShowBiteDataConsoleCommand
from ua.khpi.vydrya.main.command.console.ShowConsolePowCommand import ShowConsolePowCommand
from ua.khpi.vydrya.main.command.console.ShowHexConsoleCommand import ShowHexConsoleCommand
from ua.khpi.vydrya.main.command.impl.AndBiteOperationsCommand import AndBiteOperationsCommand
from ua.khpi.vydrya.main.command.impl.CastToIntCommand import CastToIntCommand
from ua.khpi.vydrya.main.command.impl.PowCommand import PowCommand

if __name__ == "__main__":
    help("abs")

    data = Data()
    command = ConsoleCommand()
    command.add_item(input_command, ConsoleInputCommand(data))
    command.add_item(pow_command, PowCommand(data))
    command.add_item(show_pow, ShowConsolePowCommand(data))
    command.add_item(cast_to_int_command, CastToIntCommand(data))
    command.add_item(show_hex_console_command, ShowHexConsoleCommand(data))

    bite_data = BiteData()
    command.add_item(input_bite_command, ConsoleInputShiftCommand(bite_data))
    command.add_item(and_bite_operations_command, AndBiteOperationsCommand(bite_data))
    command.add_item(show_bite_data_command, ShowBiteDataConsoleCommand(bite_data))

    command.execute()

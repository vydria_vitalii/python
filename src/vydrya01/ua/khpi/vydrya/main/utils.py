from typing import Union
import re


def check_type(input: Union[int, float], pattern) -> bool:
    return re.match(pattern, str(input)) is not None

__all__ = ["regex_float", "utils", "regex_int", "regex_octal", "regex_max_byte"]

regex_float = r'^[-+]?[0-9]*\.[0-9]+([eE][-+]?[0-9]+)?$'
regex_int = r'^[-+]?(([1-9]\d+)|(\d))$'
regex_max_byte = r'^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$'
regex_octal = r'^0[1-7][0-7]*$'

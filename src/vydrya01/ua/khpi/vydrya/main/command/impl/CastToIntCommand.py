from ua.khpi.vydrya.main import Data, utils, regex_float
from ua.khpi.vydrya.main.command.Command import Command


class CastToIntCommand(Command):
    def __init__(self, data: Data):
        self.__data = data

    def execute(self):
        if utils.check_type(self.__data.c, regex_float):
            self.__data.c = int(self.__data.c)

from ua.khpi.vydrya.main import Data
from ua.khpi.vydrya.main.command.Command import Command


class PowCommand(Command):
    def __init__(self, data: Data):
        self.__data = data

    def execute(self):
        self.__data.c = (self.__data.a ** self.__data.b)

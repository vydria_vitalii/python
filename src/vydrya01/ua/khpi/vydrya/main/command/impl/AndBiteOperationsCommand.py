from ua.khpi.vydrya.main import BiteData
from ua.khpi.vydrya.main.command.Command import Command


class AndBiteOperationsCommand(Command):
    def __init__(self, data: BiteData):
        self.__data: BiteData = data

    def execute(self):
        self.__data.c = self.__data.a & self.__data.b

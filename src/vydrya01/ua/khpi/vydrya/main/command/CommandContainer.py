from typing import Dict

from ua.khpi.vydrya.main.command import Command


class CommandContainer:
    def __init__(self):
        self.__items: Dict[str, Command] = dict()

    def get_item(self, key: str) -> Command:
        return self.__items[key]

    def add_item(self, key: str, value: Command):
        self.__items[key] = value

    def _get_items(self):
        return self.__items

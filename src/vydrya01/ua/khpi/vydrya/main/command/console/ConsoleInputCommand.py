from ua.khpi.vydrya.main import utils, regex_int, regex_octal
from ua.khpi.vydrya.main.Data import Data
from ua.khpi.vydrya.main.command.Command import Command
from ua.khpi.vydrya.main.command.console import enter_int, enter_octal, print_a_b


class ConsoleInputCommand(Command):
    def __init__(self, data: Data):
        self.__flag_input: bool = None
        self.__data: Data = data

    def __inp_console(self, pattern, message):
        inp_text = input(message)
        self.__flag_input = utils.check_type(inp_text, pattern)

        if self.__flag_input:
            return inp_text
        else:
            return None

    def execute(self):
        str_tmp = self.__inp_console(regex_int, enter_int)
        while not self.__flag_input:
            str_tmp = self.__inp_console(regex_int, enter_int)
        self.__data.a = int(str_tmp)

        str_tmp = self.__inp_console(regex_octal, enter_octal)
        while not self.__flag_input:
            str_tmp = self.__inp_console(regex_octal, enter_octal)
        self.__data.b = int(str_tmp, 8)

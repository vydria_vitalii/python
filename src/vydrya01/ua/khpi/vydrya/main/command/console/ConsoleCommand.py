from typing import Dict

from ua.khpi.vydrya.main.command.Command import Command
from ua.khpi.vydrya.main.command.CommandContainer import CommandContainer


class ConsoleCommand(CommandContainer, Command):
    def execute(self):
        command_items: Dict[str, Command] = self._get_items()
        for key, value in command_items.items():
            value.execute()

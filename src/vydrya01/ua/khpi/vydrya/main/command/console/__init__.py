__all__ = ["enter_int", "enter_octal", "print_a_b", "print_a_b_c", "print_c_hex", "enter_byte_value",
           "enter_shift_value", "print_bite_operation"]

enter_int = "Enter int number: a = "
enter_octal = "Enter octal number: b = "
enter_byte_value = "Enter byte value(0-255) = "
enter_shift_value = "Enter shift value(0-255) = "
print_a_b = "a = {0}\nb = {1}"
print_a_b_c = "a = {0}\nb = {1}\nc(a**b) = {2}"
print_c_hex = "c = {0} hex = {1}"
print_bite_operation = "a = {0}, b = {1}, c(a & b) = {2}"

from typing import Union

from ua.khpi.vydrya.main import BiteData, utils, regex_max_byte
from ua.khpi.vydrya.main.command.Command import Command
from ua.khpi.vydrya.main.command.console import enter_byte_value, enter_shift_value


class ConsoleInputShiftCommand(Command):
    def __init__(self, data: BiteData):
        self.__data: BiteData = data
        self.__flag_input: bool = None

    def __input_data(self, text, pattern) -> Union[str, None]:
        inp_text = input(text)
        self.__flag_input = utils.check_type(inp_text,pattern)

        if self.__flag_input:
            return inp_text
        else:
            return None

    def execute(self):
        str_tmp = self.__input_data(enter_byte_value, regex_max_byte)
        while not self.__flag_input:
            str_tmp = self.__input_data(enter_byte_value, regex_max_byte)
        self.__data.a = int(str_tmp)

        str_tmp = self.__input_data(enter_shift_value, regex_max_byte)
        while not self.__flag_input:
            str_tmp = self.__input_data(enter_shift_value, regex_max_byte)
        self.__data.b = int(str_tmp)

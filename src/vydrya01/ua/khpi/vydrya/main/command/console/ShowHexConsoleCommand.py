from ua.khpi.vydrya.main import Data
from ua.khpi.vydrya.main.command.Command import Command
from ua.khpi.vydrya.main.command.console import print_c_hex


class ShowHexConsoleCommand(Command):
    def __init__(self, data: Data):
        self.__data = data

    def execute(self):
        print(print_c_hex.format(self.__data.c, hex(self.__data.c)))

from ua.khpi.vydrya.main import Data
from ua.khpi.vydrya.main.command.Command import Command
from ua.khpi.vydrya.main.command.console import print_a_b_c


class ShowConsolePowCommand(Command):
    def __init__(self, data: Data):
        self.__data = data

    def execute(self):
        print(print_a_b_c.format(self.__data.a, self.__data.b, self.__data.c))

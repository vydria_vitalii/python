from ua.khpi.vydrya.main import BiteData
from ua.khpi.vydrya.main.command.Command import Command
from ua.khpi.vydrya.main.command.console import print_bite_operation


class ShowBiteDataConsoleCommand(Command):
    def __init__(self, data: BiteData):
        self.__data: BiteData = data

    def execute(self):
        print(print_bite_operation.format(self.__data.a, self.__data.b, self.__data.c))
